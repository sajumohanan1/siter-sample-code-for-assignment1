﻿namespace DataTransferObjects_SwaggerDoc.DataTransferObjects.StudentDTO
{
    //For reading data from database to the application
    public class DTOReadStudent
    {        
        public string FirstName { get; set; }

        public string LastName { get; set; }
       
    }
}
