﻿using DataTransferObjects_SwaggerDoc.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataTransferObjects_SwaggerDoc.DataTransferObjects.StudentDTO
{
    //This class is for DTO
    //You are going to create new records for the student
    //exposing only relevant information, and hiding other properties (fields)
    public class DTOCreateStudent
    {        
        public string FirstName { get; set; }

        public string LastName { get; set; }        

        //add the foreign key
        public int ProfessorId { get; set; }
       
    }
}
