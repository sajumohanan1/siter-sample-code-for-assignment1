﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataTransferObjects_SwaggerDoc.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        [Column(TypeName ="Date")] //provide only valid date (YYYY-MM-DD)
        public DateTime DOB { get; set; }

        //add the foreign key
        public int ProfessorId { get; set; }

        //Navigation property (many side)
        public Professor Professor { get; set; }    
        
    }
}
