﻿namespace DataTransferObjects_SwaggerDoc.Models
{
    public class Professor
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Subject { get; set; }
        public double Salary { get; set; }
        public string Rank { get; set; }
        public string Department { get; set; }
        public string Location { get; set; }

        //Navigation property: professor has many students
        public ICollection<Student> Students { get; set; }
    }
}
