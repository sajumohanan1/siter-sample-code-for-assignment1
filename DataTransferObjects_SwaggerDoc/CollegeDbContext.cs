﻿using DataTransferObjects_SwaggerDoc.Models;
using Microsoft.EntityFrameworkCore;

namespace DataTransferObjects_SwaggerDoc
{
    public class CollegeDbContext : DbContext
    {
        //Constructor - options
        public CollegeDbContext(DbContextOptions options) : base(options)
        {
        }

        //Physical tables
        public DbSet<Professor> Professors { get; set; }
        public DbSet<Student> Students { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Professor>().HasData(DataSeed.GetProfessors());
            modelBuilder.Entity<Student>().HasData(DataSeed.GetStudents());

        }
    }
}
