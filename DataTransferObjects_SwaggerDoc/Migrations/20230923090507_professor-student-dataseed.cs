﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataTransferObjects_SwaggerDoc.Migrations
{
    public partial class professorstudentdataseed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Professors",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Subject = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Salary = table.Column<double>(type: "float", nullable: false),
                    Rank = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Department = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Location = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Professors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DOB = table.Column<DateTime>(type: "Date", nullable: false),
                    ProfessorId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Students_Professors_ProfessorId",
                        column: x => x.ProfessorId,
                        principalTable: "Professors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Professors",
                columns: new[] { "Id", "Department", "FirstName", "LastName", "Location", "Rank", "Salary", "Subject" },
                values: new object[] { 1, "IT", "Saju", "Mohanan", "Norway", "Lecturer", 500.10000000000002, "IT" });

            migrationBuilder.InsertData(
                table: "Professors",
                columns: new[] { "Id", "Department", "FirstName", "LastName", "Location", "Rank", "Salary", "Subject" },
                values: new object[] { 2, "Business", "Stephen", "Krish", "USA", "Professor", 550.20000000000005, "Management" });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "Id", "DOB", "FirstName", "LastName", "ProfessorId" },
                values: new object[] { 1, new DateTime(2022, 10, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sam", "Daniel", 1 });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "Id", "DOB", "FirstName", "LastName", "ProfessorId" },
                values: new object[] { 2, new DateTime(2017, 11, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mike", "Stian", 1 });

            migrationBuilder.CreateIndex(
                name: "IX_Students_ProfessorId",
                table: "Students",
                column: "ProfessorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Students");

            migrationBuilder.DropTable(
                name: "Professors");
        }
    }
}
