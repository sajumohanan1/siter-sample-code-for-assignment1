﻿using AutoMapper;
using DataTransferObjects_SwaggerDoc.DataTransferObjects.StudentDTO;
using DataTransferObjects_SwaggerDoc.Models;

namespace DataTransferObjects_SwaggerDoc.Profiles
{
    public class StudentProfile : Profile
    {
        //Open the default constructor
        public StudentProfile()
        {

            /*DTOReadStudent - retrive student details from student table (domain)
             * and present StudentDTO to the clinet
             * Mapping data from student to DTOReadStudent
             */
            CreateMap<Student, DTOReadStudent>();


            /* Create student object, client provide StudentDTO 
             * Student table receives the newly created objects
             * such objects will store in the actual model 'Student'
             */
            CreateMap<DTOCreateStudent, Student>();
        }           


        

    }
}
