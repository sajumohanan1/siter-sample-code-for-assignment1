﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DataTransferObjects_SwaggerDoc;
using DataTransferObjects_SwaggerDoc.Models;
using AutoMapper;
using DataTransferObjects_SwaggerDoc.DataTransferObjects.StudentDTO;

namespace DataTransferObjects_SwaggerDoc.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Consumes("application/json")]
    public class StudentsController : ControllerBase
    {
        #region Constructor

        private readonly CollegeDbContext _context;
        private readonly IMapper _mapper;

        public StudentsController(CollegeDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        #endregion



        /// <summary>
        /// Get all the students from the Student
        /// </summary>
        /// <returns>List of Student DTOS to the client</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // GET: api/Students
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTOReadStudent>>> GetStudents()
        {
          if (_context.Students == null)
          {
              return NotFound();
          }
          var studentModel =  await _context.Students.ToListAsync();
            var studentDto = _mapper.Map<List<DTOReadStudent>>(studentModel);
            return studentDto;
        }

        /// <summary>
        /// Get a single student by searching the ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Return only a single student</returns>
        // GET: api/Students/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DTOReadStudent>> GetStudent(int id)
        {
          if (_context.Students == null)
          {
              return NotFound();
          }
            var student = await _context.Students.FindAsync(id);

            if (student == null)
            {
                return NotFound();
            }

            return _mapper.Map<DTOReadStudent>(student);
        }

        // PUT: api/Students/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutStudent(int id, Student student)
        {
            if (id != student.Id)
            {
                return BadRequest();
            }

            _context.Entry(student).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StudentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Students
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<DTOReadStudent>> PostStudent(DTOCreateStudent studentDto)
        {
          if (_context.Students == null)
          {
              return Problem("Entity set 'CollegeDbContext.Students'  is null.");
          }
          var studentModel = _mapper.Map<Student>(studentDto);

            _context.Students.Add(studentModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetStudent", new { id = studentModel.Id }, studentDto);
        }

        // DELETE: api/Students/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteStudent(int id)
        {
            if (_context.Students == null)
            {
                return NotFound();
            }
            var student = await _context.Students.FindAsync(id);
            if (student == null)
            {
                return NotFound();
            }

            _context.Students.Remove(student);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool StudentExists(int id)
        {
            return (_context.Students?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
