﻿using DataTransferObjects_SwaggerDoc.Models;

namespace DataTransferObjects_SwaggerDoc
{
    public class DataSeed
    {
        public static List<Student> GetStudents()
        {
            List<Student> students = new List<Student>()
            {
                new Student()
                {
                    Id= 1,
                    FirstName= "Sam",
                    LastName= "Daniel",
                    DOB = new DateTime(2022,10,10),
                    ProfessorId = 1 //FK
                },
                new Student()
                {
                    Id= 2,
                    FirstName= "Mike",
                    LastName= "Stian",
                    DOB = new DateTime(2017,11,10),
                    ProfessorId = 1 //FK
                }
            };
            return students;
        }
        public static List<Professor> GetProfessors()
        {
            List<Professor> professors = new List<Professor>()
            {
                new Professor()
                {
                    Id= 1,
                    FirstName= "Saju",
                    LastName= "Mohanan",
                    Subject= "IT",
                    Salary= 500.100,
                    Rank= "Lecturer",
                    Department= "IT",
                    Location= "Norway"
                },
                new Professor()
                {
                    Id= 2,
                    FirstName= "Stephen",
                    LastName= "Krish",
                    Subject= "Management",
                    Salary= 550.200,
                    Rank= "Professor",
                    Department= "Business",
                    Location= "USA"
                }
            };
            return professors;
        }
    }
}
